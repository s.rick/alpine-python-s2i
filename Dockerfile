FROM python:3.7-alpine

LABEL summary="Base python alpine image which allows the use of s2i" \
      description="s2i image building on alpine python. This image tries to be as small als possible while providing all for s2i needed requirements" \
      io.k8s.description="s2i image building on alpine python. This image tries to be as small als possible while providing all for s2i needed requirements" \
      io.k8s.display-name="alpine-python-s2i" \
      # labels for s2i and openshift where to find the s2i scripts
      io.openshift.s2i.scripts-url=image:///usr/libexec/s2i \
      io.s2i.scripts-url=image:///usr/libexec/s2i \
      name="alpine-python-s2i" \
      version="0"

ENV APP_ROOT=/opt/app-root \
    # DEPRECATED: Use above labels instead
    STI_SCRIPTS_URL=image:///usr/libexec/s2i \
    APP_ROOT=/opt/app-root \
    HOME=/opt/app-root/src \
    # extend path with home directory
    PATH=/opt/app-root/src/bin:/opt/app-root/bin:$PATH \
    PIP_NO_CACHE_DIR=1



# copy s2i basic files and utility scripts
COPY ./root/ /

WORKDIR ${HOME}
ENTRYPOINT ["container-entrypoint"]
CMD ["base-usage"]

# create user openshift uses while s2i, make the app_root his and change utitilty access
RUN adduser -u 1001 -S -G root -h ${HOME} -s /sbin/nologin default && \
    chown -R 1001:0 ${APP_ROOT} && \
    apk update && apk add findutils && rm -rf /var/cache/apk/* && \
    chmod 777 /usr/libexec/s2i/* && \
    chmod 777 /usr/bin/base-usage && \
    chmod 777 /usr/bin/container-entrypoint && \
    chmod 777 /usr/bin/fix-permissions && \
    fix-permissions ${APP_ROOT}